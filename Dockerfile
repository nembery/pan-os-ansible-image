FROM python:3.7-alpine

LABEL description="Minimal Pantools"
LABEL version="0.2"
LABEL maintainer="nembery@paloaltonetworks.com"

WORKDIR /app

ENV PATH="/app:${PATH}"
ENV PYTHONUNBUFFERED=1

COPY requirements.txt /app/requirements.txt

# Install most everything here
RUN apk add --update --no-cache curl libxml2 libxml2-dev libxslt-dev libffi-dev openssl-dev git && \
    apk add --virtual build-dependencies build-base && \
	pip install --upgrade pip && \
	pip install --no-cache-dir -r requirements.txt && \
    apk del build-dependencies

# install paloalonetwork ansible roles here
#RUN ansible-galaxy install PaloAltoNetworks.paloaltonetworks --ignore-certs
RUN git clone https://github.com/nembery/pan-os-ansible.git -c http.sslVerify=false && \
    ansible-galaxy collection build pan-os-ansible && \
    ansible-galaxy collection install paloaltonetworks-panos-1.1.0.tar.gz

